https://github.com/ansible/ansible/issues/11620

- name: super-ugly hack to allow unauthenticated packages to install
  copy: content='APT::Get::AllowUnauthenticated "true";' dest=/etc/apt/apt.conf.d/99temp owner=root group=root mode=0644

 - name: remove hack that allows unauthenticated packages to install
   file: path=/etc/apt/apt.conf.d/99temp state=absent